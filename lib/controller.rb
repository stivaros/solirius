require_relative 'controller/timer.rb'
require_relative 'controller/colours.rb'
require_relative 'controller/output.rb'
require_relative 'controller/status.rb'

class Controller
    
    def initialize(num_of_lights)
        puts "Press Ctrl + C on your keyboard to end this program at any time"
        @num_of_lights = validate(num_of_lights)
        @colours = Colours.new
        @timer = Timer.new
        @position = 1
        @status = Status.new
    end
    
    def run
        @timer.output_per_second(1) do
            if @position == (@num_of_lights + 1)
                break
            else
                output = Output.new((Time.now.strftime("%H:%M:%S")), @position, @colours.get_colour(@position), @status.retrieve)
                p output.get_message
            end
            @status.update()
            update_number()
        end
    end
        
    

    private
    
    def validate(num_of_lights)
        if num_of_lights == nil
            puts "Integer not detected, defaulting to 20"
            return 20
        end
        Integer(num_of_lights)
        rescue ArgumentError
            puts "Integer not detected, defaulting to 20"
            return 20
    end
    
    def update_number()
        @position += 1 if @status.retrieve == "On"
    end
            

end