class Output
  
  def initialize(time, position, colour, status)
   @time = time
   @position = position
   @colour = colour
   @status = status
  end
  
  def get_message
    "#{@time}: #{@position} #{@colour} Light #{@status}"
  end
  
end