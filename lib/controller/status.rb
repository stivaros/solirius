class Status
  def initialize
    @current_status = true
  end
  
  def update()
      if @current_status == true
          @current_status = false
      else
          @current_status = true
      end
  end
  
  def retrieve()
    if @current_status == true
      "On"
    else
      "Off"
    end
  end
  
end