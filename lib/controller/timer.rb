class Timer
    
    def output_per_second(seconds)
      last_tick = Time.now
      loop do
        sleep 0.1
        if Time.now - last_tick >= seconds
          last_tick += seconds
          yield
        end
      end
    end
    
end