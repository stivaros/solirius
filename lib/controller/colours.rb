class Colours
    
    def initialize
        @colours = ["Red", "Green", "White"] # Add new colours to this array before running the program.
    end
    
    def get_colour(position)
        @colours[(position % @colours.length) - 1]
    end
    
    
end