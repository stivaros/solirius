# How To Use

This app has no dependencies and runs in the command line. Simply navigate to the folder with the file `app.rb` and run `ruby app.rb`. Running the app in this manner defaults to 20 lights.

Alternatively, by running `ruby app.rb <integer>` the program will run for `<integer>` number of lights. Entering a non-integer value as `<integer>` will run a default 20 lights.

# The Problem

Write a ~~Java~~ Ruby program to turn on/off a set of coloured lights.

The number of lights should be configurable on the command line with a default value of 20.

The set of lights will be made up of a repeating set of colours in a fixed order (e.g red, green, white)

When you run the program, it should turn each light in the set on for 1 second and then turn it off.

The output of the program should be formatted as follows: {HH:MM:SS} {light no / postion} {colour} Light {light state}

12:00:01: 1 Red Light On 12:00:02: 1 Red Light Off 12:00:02: 2 Green Light On 12:00:03: 2 Green Light Off 12:00:03: 3 White Light On 12:00:04: 3 White Light Off etc. etc.

The sequence should continue in a loop until the program is interrupted.

Design the program so that it would be easy to modify the colours and the number of colours that make up the set of lights using object oriented techniques. (e.g. blue, red, yellow, white).